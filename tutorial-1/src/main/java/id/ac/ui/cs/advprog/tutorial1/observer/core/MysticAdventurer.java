package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        this.guild = guild;
    }

    @Override
    public void update() {
        if(guild.getQuestType().equals("D") || guild.getQuestType().equals("E")){
            addQuests(guild.getQuest());
        }
    }
}
