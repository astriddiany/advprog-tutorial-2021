package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        //ToDo: Complete Me
        this.guild = guild;

    }

    @Override
    public void update() {
        if(guild.getQuestType().equals("D") || guild.getQuestType().equals("R")){
            addQuests(guild.getQuest());
        }
    }
}
