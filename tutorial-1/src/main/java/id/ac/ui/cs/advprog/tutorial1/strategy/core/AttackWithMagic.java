package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {

    @Override
    public String attack() {
        return "Kesihir!!";
    }

    @Override
    public String getType() {
        return "Ini pakai Magic!";
    }

}
